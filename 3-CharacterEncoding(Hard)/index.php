<?php 

// Impossible without these variables.
$cipher = "aes-128-cbc"; 
$key = file_get_contents('key');

// Init the database connection
$db_host = 'localhost'; // You might need to use localhost
$database = 'test_db'; 
$db_user = 'root';
$db_pwd = ''; // You might not have a root password

$pdo = new PDO("mysql:host=$db_host;dbname=$database", $db_user, $db_pwd);
$pdo->setAttribute(PDO::ATTR_ERRMODE,  PDO::ERRMODE_EXCEPTION); 

$raw_users = $pdo->query('SELECT * FROM test_db.users')->fetchAll();
$all_users = array();

foreach ( $raw_users as $user )
{
    //Update local user array for front end display
    $all_users[] = array(
        'uid' => $user['uid'],
        'ufirstname' => CheckAndFix( openssl_decrypt($user['ufirstname'], $cipher, $key) ),
        'ulastname'  => CheckAndFix( openssl_decrypt($user['ulastname'], $cipher, $key) )
        
    );

    //Update the database with fixed names
    $id = $user['uid'];
    $first = openssl_encrypt(
        CheckAndFix( openssl_decrypt($user['ufirstname'], $cipher, $key) ),
        $cipher,
        $key);
    $last = openssl_encrypt(
        CheckAndFix( openssl_decrypt($user['ulastname'], $cipher, $key) ),
        $cipher,
        $key);
    
    $sql = "
    UPDATE 
        test_db.users
    SET 
        ufirstname=:first,
        ulastname=:last
    WHERE 
        uid=:id";
    $stm = $pdo->prepare($sql);
    $stm->bindParam(':first', $first, PDO::PARAM_STR);
    $stm->bindParam(':last', $last, PDO::PARAM_STR);
    $stm->bindParam(':id', $id, PDO::PARAM_INT);

    $stm->execute(); 

}

//Display localy fixed names
echo "<pre>";
print_r($all_users);
echo "</pre>";

//Change first and last name from ISO-8859-15 to UTF-8 
function CheckAndFix($name){

    if(mb_detect_encoding($name,'UTF-8, ISO-8859-15,ISO-8859-1',true) === 'ISO-8859-15')
    {
        echo "Fixing string";
        return mb_convert_encoding($name,'UTF-8','ISO-8859-1');
    }
    else 
    {
        return $name;
    }
    
}

?>
