# LibreSoft PHP/JS tests #

Below please find the sources and any comments for each test. For descriptions of what and why I did things please read the commit messages

### Setting Up ###
* Installing XAMPP
	* https://www.youtube.com/watch?v=mXdpCRgR-xE&ab_channel=DaniKrossing


### Ajax ###
* Changing html element contents test
	* https://www.youtube.com/watch?v=98IIhGX8fYM&ab_channel=frankie2Tone
* XMLHttpRequest 
	* https://www.w3schools.com/js/js_json_php.asp
* php arrays
	* https://www.w3schools.com/php/php_arrays.asp
* Invalid character chr(193)
	* https://alexwebdevelop.com/php-json-backend/
	* https://thecodersblog.com/JSON-error/




### FizzBuzz ###
* Fixing basic mistakes, no sources were used. However I have seen a Youtube video recently which covered this topic.
	* https://www.youtube.com/watch?v=QPZ0pIK_wsc&ab_channel=TomScott


### Character Encoding ###
* Create php function
	* https://www.w3schools.com/php/php_functions.asp
* Checking string containing specific character
	* https://joshtronic.com/2015/10/25/check-if-a-string-contains-a-character/
* Issue Character Encoding? Trying to find what the character should have been...
	* https://support.mozilla.org/en-US/questions/1169587#answer-991744
	* https://www.php.net/manual/en/function.mb-ord.php
	* https://www.php.net/manual/en/function.ord.php
	* https://www.codetable.net/decimal/233 
* Fixed Character Encoding. Now echos the correct encoded string. 
	* https://www.php.net/manual/en/function.mb-convert-encoding.php
	* https://www.php.net/manual/en/mbstring.supported-encodings.php
	* https://www.php.net/manual/en/function.mb-detect-encoding.php
* Writing to the Database
	* https://www.tutorialrepublic.com/sql-tutorial/sql-cloning-tables.php
	* https://stackoverflow.com/questions/37269899/php-pdo-update-query-with-variables
	* https://www.php.net/manual/en/function.openssl-encrypt.php




