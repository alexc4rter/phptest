
(function(factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery2'], factory);
    } else if (typeof module !== 'undefined' && module.exports) {
        module.exports = factory(require('jquery2'));
    } else {
        factory(jQuery);
    }
}(function($, undefined) {

    $('#button').click(function(e){

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            try{
                myObj =  JSON.parse(this.responseText);
                document.getElementById("response_1").innerHTML = myObj['response_1'];
                document.getElementById("response_2").innerHTML = myObj['response_2'];
                document.getElementById("error_message").innerHTML = "";
            } catch (e){
                console.log(e);
                document.getElementById("error_message").innerHTML = "Unexpected but expectecd error!";
            }
          }
        };
        xmlhttp.open("GET", "internal_script.php", true);
        xmlhttp.send();
        //alert('Clicked');

    });


}));