<html>
    <body style='font-family:Arial;width:800px;margin-left:auto;margin-right:auto;margin-top:50px;display:block'>
        <p>When you click 'Refresh Responses', the two strings 'Response 1' and 'Response 2', should be updated with the values returned from internal_script.php</p>
        <p>This page should not refresh, and internal_script.php should not be edited (viewing is fine).</p>
        <p><b>You will need to rename script.notajsfile to script.js</b></p> 
        <p>You'll know its working when an alert is triggered by clicking the 'Refresh Responses' button.</p>
        <br><br>
        <div id="response_1">
            Response 1
        </div>
        
        <br>
        <br>

        <div id="response_2">
            Response 2
        </div>

        <br>
        <br>
        <button id="button" >Refresh Responses</button>
        <b id="error_message" style="color:red"></b>

    </body>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src='script.js'></script>
</html>