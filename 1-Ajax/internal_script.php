<?php 

$responses = array(
    'Hello world!',
    'Goodbye world!',
    'PHP stands for PHP',
    'JavaScript or Javascript?',
    chr(193),
    'Wordpress or WordPress?',
    'One ring to rule them all?'
);

$keys = array_rand($responses, 2);
$output = array();
$output['response_1'] = $responses[$keys[0]];
$output['response_2'] = $responses[$keys[1]];

echo json_encode($output);

?>
